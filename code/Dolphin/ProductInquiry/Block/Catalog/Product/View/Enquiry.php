<?php

namespace Dolphin\ProductInquiry\Block\Catalog\Product\View;

use Magento\Store\Model\ScopeInterface;

class Enquiry extends  \Magento\Framework\View\Element\Template
{
    private $resultPageFactory;
    protected $registry;
    protected $_session;
    protected $authSession;

    public function __construct(
        \Magento\Customer\Model\Session $session,
        \Magento\Backend\Model\Auth\Session $authSession,
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        parent::__construct($context);
        $this->_session = $session;
        $this->authSession = $authSession;
        $this->scopeConfig = $scopeConfig;
        $this->registry = $registry;
    }
    public function getCurrentUserEmail()
    {
        return $this->_session->getCustomer()->getEmail();
    }
    public function getCurrentUserName()
    {
        return $this->_session->getCustomer()->getName();
    }
    public function getUserStatus()
    {
        return $this->_session->isLoggedIn();
    }
    public function getModuleStatus()
    {
        $status = $this->scopeConfig->getValue('productinquiry/general_option/enable_module', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $status;
    }
    public function getFormTitle()
    {
        $status = $this->scopeConfig->getValue('productinquiry/general_option/form_title', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $status;
    }
    public function getButtonTabOption()
    {
        $status = $this->scopeConfig->getValue('productinquiry/general_option/form_type', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $status;
    }
    public function getButtonTabTitle()
    {
        $status = $this->scopeConfig->getValue('productinquiry/general_option/tab_button_title', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $status;
    }
    public function getGuestUserStatus()
    {
        $status = $this->scopeConfig->getValue('productinquiry/general_option/allow_guest', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $status;
    }
    public function getCaptchaReCaptchaType()
    {
        $status = $this->scopeConfig->getValue('productinquiry/captcha_setting/used_captcha_type',ScopeInterface::SCOPE_STORE);
        return $status;
    }
    public function getCaptchaReCaptchaStatus()
    {
        $status = $this->scopeConfig->getValue('productinquiry/captcha_setting/captcha_enable',ScopeInterface::SCOPE_STORE);
        return $status;
    }
    public function getFileAttachmentStatus()
    {
        $status = $this->scopeConfig->getValue('productinquiry/email_setting/allow_attach',ScopeInterface::SCOPE_STORE);
        return $status;
    }
    public function getFileAttachmentExtension()
    {
        $status = $this->scopeConfig->getValue('productinquiry/email_setting/allow_attach_extension',ScopeInterface::SCOPE_STORE);
        return $status;
    }
    public function getCaptchaSiteKey()
    {
        $status = $this->scopeConfig->getValue('msp_securitysuite_recaptcha/general/public_key',ScopeInterface::SCOPE_STORE);
        return $status;
    }
    public function getCaptchaSecretKey()
    {
        $status = $this->scopeConfig->getValue('msp_securitysuite_recaptcha/general/private_key',ScopeInterface::SCOPE_STORE);
        return $status;
    }
    public function getCaptchaFormStatusSecurity()
    {
        $status = $this->scopeConfig->getValue('msp_securitysuite_recaptcha/frontend/enabled',ScopeInterface::SCOPE_STORE);
        return $status;
    }
    public function getCaptchaType()
    {
        $status = $this->scopeConfig->getValue('msp_securitysuite_recaptcha/frontend/type',ScopeInterface::SCOPE_STORE);
        return $status;
    }
    public function getCaptchaTheme()
    {
        $status = $this->scopeConfig->getValue('msp_securitysuite_recaptcha/frontend/theme',ScopeInterface::SCOPE_STORE);
        return $status;
    }
    public function getCaptchaLanguage()
    {
        $status = $this->scopeConfig->getValue('msp_securitysuite_recaptcha/frontend/lang',ScopeInterface::SCOPE_STORE);
        return $status;
    }
    public function getCaptchaSize()
    {
        $status = $this->scopeConfig->getValue('msp_securitysuite_recaptcha/frontend/size',ScopeInterface::SCOPE_STORE);
        return $status;
    }
    public function getCaptchaPosition()
    {
        $status = $this->scopeConfig->getValue('msp_securitysuite_recaptcha/frontend/position',ScopeInterface::SCOPE_STORE);
        return $status;
    }
    public function getCaptchaFormStatusCustomer()
    {
        $status = $this->scopeConfig->getValue('customer/captcha/enable', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $status;
    }
    public function getCaptchaFormSelect()
    {
        $status = $this->scopeConfig->getValue('customer/captcha/forms', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $status;
    }
    public function getCurrentProduct()
    {
        return $this->registry->registry('current_product');
    }
    public function setTabTitle($title)
    {
        $this->setTitle($title);
    }
}
