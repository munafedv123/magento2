<?php

namespace Dolphin\ProductInquiry\Block\Adminhtml\Productinquiry\Edit\Button;

use Magento\Backend\Block\Widget\Context;
use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class EditButton extends GenericButton implements ButtonProviderInterface
{
    protected $context;

    public function __construct(
        Context $context
    ) {
        $this->context = $context;
    }
    public function getButtonData()
    {
        $data = [];
        $id = $this->context->getRequest()->getParam('id');
        if ($id) {
            $data = [
                'label' => __('Edit Inquiry'),
                'class' => 'edit primary',
                'on_click' => 'deleteConfirm(\'' . __(
                    'Are you sure you want to Edit this?'
                ) . '\', \'' . $this->getEditUrl() . '\')',
                'sort_order' => 20,
            ];
        }
      return $data;
    }
    public function getEditUrl()
    {
        $id = $this->context->getRequest()->getParam('id');
        return $this->getUrl('*/*/editinquiry', ['id' => $id]);
    }
}
