<?php

namespace Dolphin\ProductInquiry\Api\Data;

interface InquiryInterface
{
    const ID = 'id';
    const PRD_NAME = 'prd_name';
    const USR_NAME = 'usr_name';
    const EMAIL = 'email';
    const SUBJECT = 'subject';
    const INQ_MSG = 'inq_msg';
    const PUBLISH_DATETIME = 'publish_datetime';
    const UPDATE_DATETIME = 'update_datetime';

    public function getId();
    public function setId($Id);
    public function getPrdName();
    public function setPrdName($prd_name);
    public function getUsrName();
    public function setUsrName($usr_name);
    public function getEmail();
    public function setEmail($email);
    public function getSubject();
    public function setSubject($subject);
    public function getInqMsg();
    public function setInqMsg($inq_msg);
    public function getPublishDatetime();
    public function setPublishDatetime($publish_datetime);
    public function getUpadteDatetime();
    public function setUpadteDatetime($update_datetime);
}