<script>
require(['jquery', 'jquery/ui'], function($){
	$(document).ready( function() {
	
	$(".product-items").children("li").each(function() {
	
	var imageclass = ".product-image-photo";
    var imageflip = "Y";
    var flipper1 = $(this).find('#flipimage').attr('name', 'flipid').text();
    //var partDesc1 = $('#flipimage').attr('name', 'flipid').closest('.product-item-info').find('.product-image-photo').attr('src');

     if (flipper1 != '') { 
        $(this).find('#flipimage').attr('name', 'flipid').closest('.product-item-info')
          .on("mouseenter", function() {

          var base = $('#flipimage').attr('name', 'flipid').closest('.product-item-info').find('.product-image-photo').attr('src');
          var flipper =$(this).find('#flipimage').attr('name', 'flipid').text();
              
          $('.product-item').find('#flipimage').attr('name', 'flipid').text(base);
              
           $('#flipimage').attr('name', 'flipid').closest('.product-item-info')
              .find(imageclass).css({
              'transform': 'rotate' + imageflip + '(180deg)',
              'transform-style': 'preserve-3d',
              'transition': 'all 0.5s ease-out 0s'
            });

            $(this).find('#flipimage').attr('name', 'flipid').closest('.product-item-info')
              .find(imageclass).attr('src', flipper);
              }).on("mouseleave", function() {

          var base = $('#flipimage').attr('name', 'flipid').closest('.product-item-info').find('.product-image-photo').attr('src');
          var flipper =$(this).find('#flipimage').attr('name', 'flipid').text();
           $('.product-item').find('#flipimage').attr('name', 'flipid').text(base);
              
           $('#flipimage').attr('name', 'flipid').closest('.product-item-info')
              .find(imageclass).css({
              'transform': 'rotate' + imageflip + '(0deg)',
              'transform-style': 'preserve-3d',
              'transition': 'all 0.5s ease-out 0s'
            });

            $('#flipimage').attr('name', 'flipid').closest('.product-item-info')
              .find(imageclass).attr('src', flipper);
            });
        }

   
  
});
});
});

</script>
