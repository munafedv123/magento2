<?php

namespace Dolphin\Productinquiry\Controller\Adminhtml\Productinquiry;

use Magento\Framework\Controller\ResultFactory;

class EditInquiry extends \Magento\Backend\App\Action
{
    public function execute()
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->getConfig()->getTitle()->prepend(__('Edit Inquiry'));
        return $resultPage;
    }
}