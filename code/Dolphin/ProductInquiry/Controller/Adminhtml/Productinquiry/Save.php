<?php

namespace Dolphin\Productinquiry\Controller\Adminhtml\Productinquiry;

use Magento\Store\Model\ScopeInterface;
use Magento\Framework\Controller\ResultFactory;

class Save extends \Magento\Backend\App\Action
{
    protected $_transportBuilder;
    protected $_storeManager;
    protected $product;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Catalog\Model\ProductFactory $product
    ){
        parent::__construct($context);
        $this->_transportBuilder = $transportBuilder;
        $this->_storeManager = $storeManager;
        $this->scopeConfig = $scopeConfig;
        $this->product = $product;
    }
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $update_datetime = date("Y-m-d h:i:s");
        $resultRedirect = $this->resultRedirectFactory->create();
        if (!$data)
        {
            return $resultRedirect->setPath('productinquiry/productinquiry/index');
        }
        // for Save Attechment file
        if($data['allow_attechment'] == 1)
        {
            if(isset($data['attechment_file'][0]['name']))
            {
                $data['attechment_file'] = $data['attechment_file'][0]['name'];
            }
            else
            {
                $this->messageManager->addError(__("Attechment File are Require"));
            }
        }
        else
        {
            $data['attechment_file'] = '';
        }
        // data are save here
        try {
            $rowData = $this->_objectManager->create('Dolphin\ProductInquiry\Model\Productinquiry');
            $rowData->setData($data);
            if (isset($data['id'])) {
                $rowData->setId($data['id']);
                $rowData->setUpadteDatetime($update_datetime);
            }
            $rowData->save();
            $this->SendMailCustomer();
            $this->messageManager->addSuccess(__('Row data has been successfully saved.'));
        } catch (\Exception $e) {
            $this->messageManager->addError(__($e->getMessage()));
        }
        if ($this->getRequest()->getParam('back'))
        {
            if ($this->getRequest()->getParam('back') == 'add')
            {
                return $resultRedirect->setPath('productinquiry/productinquiry/addinquiry');
            }
            else
            {
                return $resultRedirect->setPath(
                    'productinquiry/productinquiry/editinquiry',
                    [
                        'id' => $rowData->getId(),
                        '_current' => true
                    ]
                );
            }
        }
        return $resultRedirect->setPath('productinquiry/productinquiry/index');
    }
    public function SendMailCustomer()
    {
        $data = $this->getRequest()->getPostValue();
        if(($data['inquiry_status'] == 4)||($data['inquiry_status'] == 5))
        {
            if($data['inquiry_status'] == 4)
            {
                $prd_status = 'Rejected';
            }
            else
            {
                $prd_status = 'Finished';
            }
            $select_email_template = $this->getEmailTemplate();
            $store = $this->_storeManager->getStore()->getId();
            $sender = $this->getStoreEmailAddress();
            // get Product id to name
            $prd_collection = $this->product->create()->load($data['prd_name']);
            $get_prd_name =$prd_collection->getName();
            // send Email  to Cusotomer
            try
                {
                    $transport = $this->_transportBuilder->setTemplateIdentifier($select_email_template)
                        ->setTemplateOptions(['area' => 'frontend', 'store' => $store])
                        ->setTemplateVars(
                            [
                               'prd_name' => $get_prd_name,
                               'usr_name' => $data['usr_name'],
                               'email'    => $data['email'],
                               'subject'  => $data['subject'],
                               'inq_msg'  => $data['inq_msg'],
                               'inquiry_replay' =>$data['inquiry_replay'],
                               'inquiry_status' => $prd_status,
                            ]
                        )
                        ->setFrom($sender)
                        ->addTo($data['email'])
                        ->getTransport();
                    $transport->sendMessage();
                }
            catch (\Exception $e)
                {
                    $this->messageManager->addError(__('E-mail send failed, please try again.'));
                }
        }// accpt and reject Email
    }
    public function getEmailTemplate()
    {
        $email_template = $this->scopeConfig->getValue('productinquiry/email_setting/email_template', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $email_template;
    }
    public function getStoreEmailAddress()
    {
        $store_email = $this->scopeConfig->getValue('productinquiry/email_setting/store_email_list', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $store_email;
    }
}
