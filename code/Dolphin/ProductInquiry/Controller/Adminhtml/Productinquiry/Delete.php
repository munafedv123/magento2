<?php

namespace Dolphin\Productinquiry\Controller\Adminhtml\Productinquiry;

class Delete extends \Magento\Backend\App\Action
{
    public function __construct(
        \Magento\Backend\App\Action\Context $context
    ) {
        parent::__construct($context);
    }
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        if (!$id )
        {
            $this->_redirect('productinquiry/productinquiry/index');
            return;
        }
        try {
                $rowData = $this->_objectManager->create('Dolphin\ProductInquiry\Model\Productinquiry');
                $rowData->load($id);
                $rowData->delete();
                $this->messageManager->addSuccess(__('Row data has been successfully Deleted.'));
            }
        catch (\Exception $e)
            {
                $this->messageManager->addError(__($e->getMessage()));
            }
        $this->_redirect('productinquiry/productinquiry/index');
    }
}
