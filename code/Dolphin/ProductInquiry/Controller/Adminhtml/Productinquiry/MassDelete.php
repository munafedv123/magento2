<?php

namespace  Dolphin\Productinquiry\Controller\Adminhtml\Productinquiry;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Dolphin\ProductInquiry\Model\ResourceModel\Productinquiry\CollectionFactory;
use Dolphin\ProductInquiry\Model\Productinquiry;

class MassDelete extends \Magento\Backend\App\Action
{
    protected $_filter;
    protected $collectionFactory;
    protected $Productinquirymodel;

    public function __construct(
        Context $context,
        CollectionFactory $collectionFactory,
        Productinquiry $Productinquirymodel,
        Filter $filter
    ) {
        $this->_filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->Productinquirymodel = $Productinquirymodel;
        parent::__construct($context);
    }
    public function execute()
    {
        $ProductinquiryData = $this->collectionFactory->create();
        foreach ($ProductinquiryData as $value)
        {
            $templateId[]=$value['id'];
        }
        $parameterData = $this->getRequest()->getParams('id');
        $selectedAppsid = $this->getRequest()->getParams('id');
        if (array_key_exists("selected", $parameterData))
        {
            $selectedAppsid = $parameterData['selected'];
        }
        if (array_key_exists("excluded", $parameterData))
        {
            if ($parameterData['excluded'] == 'false')
            {
                $selectedAppsid = $templateId;
            }
            else
            {
                $selectedAppsid = array_diff($templateId, $parameterData['excluded']);
            }
        }
        $collection = $this->collectionFactory->create();
        $collection->addFieldToFilter('id', ['in'=>$selectedAppsid]);
        $delete = 0;
        $model=[];
        foreach ($collection as $item)
        {
            $this->deleteById($item->getId());
            $delete++;
        }
        $this->messageManager->addSuccess(__('A total of %1 Records have been deleted.', $delete));
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/index');
    }
    public function deleteById($id)
    {
        $item = $this->Productinquirymodel->load($id);
        $item->delete();
        return;
    }
}
