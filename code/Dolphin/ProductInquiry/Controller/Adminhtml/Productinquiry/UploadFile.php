<?php

namespace Dolphin\ProductInquiry\Controller\Adminhtml\ProductInquiry;

use Magento\Framework\Controller\ResultFactory;

class UploadFile extends \Magento\Backend\App\Action
{
    public $fileUploader;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Dolphin\ProductInquiry\Model\FileUploader $fileUploader
    ) {
        parent::__construct($context);
        $this->fileUploader = $fileUploader;
    }
    public function execute()
    {
        try {
            $result = $this->fileUploader->saveFileToTmpDir('attechment_file');
            $result['cookie'] = [
                'name' => $this->_getSession()->getName(),
                'value' => $this->_getSession()->getSessionId(),
                'lifetime' => $this->_getSession()->getCookieLifetime(),
                'path' => $this->_getSession()->getCookiePath(),
                'domain' => $this->_getSession()->getCookieDomain(),
            ];
        } catch (\Exception $e) {
            $result = ['error' => $e->getMessage(), 'errorcode' => $e->getCode()];
        }
        return $this->resultFactory->create(ResultFactory::TYPE_JSON)->setData($result);
    }
}