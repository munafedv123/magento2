<?php

namespace Dolphin\Productinquiry\Controller\Adminhtml\Productinquiry;

use Magento\Framework\Controller\ResultFactory;

class AddInquiry extends \Magento\Backend\App\Action
{
    private $coreRegistry;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry
    ) {

       parent::__construct($context);
       $this->coreRegistry = $coreRegistry;
    }
    public function execute()
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->getConfig()->getTitle()->prepend(__('Add Inquiry'));
        return $resultPage;
    }
}

