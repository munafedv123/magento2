<?php

namespace Dolphin\ProductInquiry\Controller\Productinquiry;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\Escaper;
use  Magento\Framework\App\Filesystem\DirectoryList;

class Save extends \Magento\Framework\App\Action\Action
{
	protected $_pageFactory;
    protected $_request;
    protected $_transportBuilder;
    protected $_storeManager;
    protected $escaper;
    protected $uploaderFactory;
 	protected $adapterFactory;
 	protected $filesystem;
 	protected $product;

    public function __construct(
        \Magento\Framework\App\Action\Context $context
        ,\Magento\Framework\App\Request\Http $request
        ,\Magento\Framework\View\Result\PageFactory $pageFactory
        ,\Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
        ,\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
        ,\Magento\Store\Model\StoreManagerInterface $storeManager
        ,\Magento\MediaStorage\Model\File\UploaderFactory $uploaderFactory
        ,\Magento\Framework\Image\AdapterFactory $adapterFactory
        ,\Magento\Framework\Filesystem $filesystem
        ,\Magento\Catalog\Model\ProductFactory $product
    )
    {
        $this->_request = $request;
        $this->_pageFactory = $pageFactory;
        $this->scopeConfig = $scopeConfig;
        $this->_transportBuilder = $transportBuilder;
        $this->_storeManager = $storeManager;
        $this->uploaderFactory = $uploaderFactory;
        $this->adapterFactory = $adapterFactory;
        $this->filesystem = $filesystem;
        $this->product = $product;
        parent::__construct($context);
    }
    public function execute()
    {	echo "<pre>";
     	$data = $this->getRequest()->getPostValue();
    	$resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        if (!$data)
		{
			$this->messageManager->addError(__('Product Inquiry Details Not Found'));
	        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
	    	return $resultRedirect;
		}
    	/* check Thank you masssage*/
    	$showmsg = $this->getSuccessMassage();
		if ($showmsg == '')
		{
			$msg = 'Product Inquiry has been successfully saved.';
		}// if show msg
		else
		{
			$msg = $showmsg;
		}// if show msg
		// file Attachement status
    	$allow_attach_status = $this->getFileAttachmentStatus();
    	if(($allow_attach_status == 1)&&(!isset($data['captcha'])))
    	{
    		try
    		{
	            $uploaderFactory = $this->uploaderFactory->create(['fileId' => 'attechment_file']);
	            // get  allow Extesnion array
	    		$attachment_extesion = $this->getFileAttachmentExtension();
	    		$attachment_extesion = explode(',', $attachment_extesion);
	            $uploaderFactory->setAllowedExtensions($attachment_extesion);
	            $imageAdapter = $this->adapterFactory->create();
	            $uploaderFactory->setAllowRenameFiles(true);
	            $uploaderFactory->setFilesDispersion(true);
	            $mediaDirectory = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA);
	            $destinationPath = $mediaDirectory->getAbsolutePath('Product_Inquiry_Attachments');
	            $result = $uploaderFactory->save($destinationPath);
	            if (!$result)
	            {
	                throw new LocalizedException(
	                     __('File cannot be saved to path: $1', $destinationPath)
	                );
	             }
	             $data['attechment_file'] = $result['file'];
        	}
        	catch (\Exception $e)
        	{
        		$this->messageManager->addError(__('Attachment not Uplaoded, Please try Agrain'));
        		$resultRedirect->setUrl($this->_redirect->getRefererUrl());
    			return $resultRedirect;
        	}
    	}// allow file attachmenet Status

		// Cpatcha Status
		$recaptcha_status =$this->getCaptchaReCaptchaStatus();
		$recatcha_enable_admin = $this->getCaptchaFormStatusSecurity();
		if(($recaptcha_status == 1)&&($recatcha_enable_admin == 1))
		{
			$recaptcha_type = $this->getCaptchaReCaptchaType();
			if($recaptcha_type == 'recaptcha')
			{
				$secret_key = $this->getCaptchaSecretKey();
				if($secret_key != '')
				{
					// for Captcha Responce Check
					if($data['g-recaptcha-response'] != '' )
					{
						// check response of recaptcha
						$secretkey = $this->getCaptchaSecretKey();
		        		$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secretkey.'&response='.$data['g-recaptcha-response']);
		        		$responseData = json_decode($verifyResponse);
		        		/* captcha success check */
			       		if($responseData->success)
			        	{
				        	try
							   {
						            $rowData = $this->_objectManager->create('Dolphin\ProductInquiry\Model\Productinquiry');
						            $rowData->setData($data);
						            $rowData->save();
						            $this->messageManager->addSuccess(__($msg));
						            $this->SendMail();
								}
							catch (\Exception $e)
			        			{
			            			$this->messageManager->addError(__('please try again. Form Not Submit'));
			        			}
		       				$resultRedirect->setUrl($this->_redirect->getRefererUrl());
		       				return $resultRedirect;
						} // if captcha success
				        else
			        	{
							$this->messageManager->addError(__('Robot verification failed, please try again.'));
							$resultRedirect->setUrl($this->_redirect->getRefererUrl());
							return $resultRedirect;
			       		} //  if captcha suceess
					} // if captcha Response
					else
					{
						$this->messageManager->addError(__('Robot verification failed, please try again.'));
						$resultRedirect->setUrl($this->_redirect->getRefererUrl());
						return $resultRedirect;
					}// if captcha Response
				} // if  secret key
				else
				{
					$this->messageManager->addError(__('Google Recaptcha Secret Key is Empty'));
	        		$resultRedirect->setUrl($this->_redirect->getRefererUrl());
	    			return $resultRedirect;
				} // if secret key
			}// if recaptcha Type
		}// if recaptcha Enable
		else
		{
			if(!isset($data['captcha']))
			{
				/* captcha is not set part */
		    	try
					{
			            $rowData = $this->_objectManager->create('Dolphin\ProductInquiry\Model\Productinquiry');
			            $rowData->setData($data);
			            //print_r($rowData->getData());
			            $rowData->save();
			            $this->messageManager->addSuccess(__($msg));
			            $this->SendMail();
			        }
				catch (\Exception $e)
					{
		    			$this->messageManager->addError(__('please try again. Form Not Submit'));
					}
				$resultRedirect->setUrl($this->_redirect->getRefererUrl());
				return $resultRedirect;
			} // for not Captcha Enable
		} // if captcha Enable
	} // main Executtion function
    public function SendMail() // for email send
    {
    	$resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
    	$data = $this->getRequest()->getPostValue();
    	// Check for Email Attechementy file name and file path
    	$allow_attach_status = $this->getFileAttachmentStatus();
    	if($allow_attach_status == 1 )
    	{
	    	$mediapath = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA);
	    	$mm = $mediapath->getAbsolutePath('Product_Inquiry_Attachments').'/'.$_FILES['attechment_file']['name'];;
	    	$attechment_name = $_FILES['attechment_file']['name'];
    	}
    	//print_r($attechment_name);
    	// get Currrent  store id
    	$store = $this->_storeManager->getStore()->getId();
    	// get Store email Address
    	$sender = $this->getStoreEmailAddress();
        /* get emamil  Template ID */
        $select_email_template = $this->getEmailTemplate();
        /* check email address and name not null */
		/* copy of Email Address */
		$admin_email_adress = $this->getAdminEmailAddress();
		$email_count= 0;
		if($admin_email_adress != '')
		{
			$admin_email_adress = explode(',', $admin_email_adress);
			$admin_email_copy_to = [];
			foreach ($admin_email_adress as $valid_email)
	        {
	        	$valid_email = preg_replace('/\s+/', '', $valid_email);
	        	if(filter_var($valid_email, FILTER_VALIDATE_EMAIL))
	        	{
	        		$admin_email_copy_to[] = $valid_email;
	        	}
	        	$email_count++;
	        }
	    }
		else
		{
			// admin Email Count
			if($email_count != 0 )
			{
				$this->messageManager->addError(__('Admin Email are Invalid or Empty.'));
            	$resultRedirect->setUrl($this->_redirect->getRefererUrl());
				return $resultRedirect;
			}
		}
		// get product id to name
		$prd_collection = $this->product->create()->load($data['prd_name']);
		$get_prd_name =$prd_collection->getName();
		/* semder send email */
		try {

				$transport = $this->_transportBuilder->setTemplateIdentifier($select_email_template)
		            ->setTemplateOptions(['area' => 'frontend', 'store' => $store])
		            ->setTemplateVars(
		                [
		                   'prd_name' => $get_prd_name,
		                   'usr_name' => $data['usr_name'],
		                   'email'    => $data['email'],
		                   'subject'  => $data['subject'],
		                   'inq_msg'  => $data['inq_msg'],
		                   'admin' =>'admin'
		                ]
		            )
		            ->setFrom($sender)
		            ->addTo($admin_email_copy_to)
		            ->getTransport();
		        $transport->sendMessage();
		    }
		catch (\Exception $e)
		    {
            	$this->messageManager->addError(__('E-mail send failed, please try again.'));
            	$resultRedirect->setUrl($this->_redirect->getRefererUrl());
				return $resultRedirect;
        	}
    }
    public function getStoreEmailAddress()
    {
    	$store_email = $this->scopeConfig->getValue('productinquiry/email_setting/store_email_list', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $store_email;
    }
    public function getAdminEmailAddress()
    {
    	$copy_email = $this->scopeConfig->getValue('productinquiry/email_setting/email_copy', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $copy_email;
    }
    public function getEmailTemplate()
    {
    	$email_template = $this->scopeConfig->getValue('productinquiry/email_setting/email_template', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $email_template;
    }
    public function getSuccessMassage()
    {
    	$showmsg = $this->scopeConfig->getValue('productinquiry/general_option/thanks_msg', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $showmsg;
    }
    public function getCaptchaReCaptchaStatus()
    {
        $status = $this->scopeConfig->getValue('productinquiry/captcha_setting/captcha_enable',ScopeInterface::SCOPE_STORE);
        return $status;
    }
    public function getCaptchaReCaptchaType()
    {
        $status = $this->scopeConfig->getValue('productinquiry/captcha_setting/used_captcha_type',ScopeInterface::SCOPE_STORE);
        return $status;
    }
    public function getFileAttachmentStatus()
    {
        $status = $this->scopeConfig->getValue('productinquiry/email_setting/allow_attach',ScopeInterface::SCOPE_STORE);
        return $status;
    }
    public function getFileAttachmentExtension()
    {
        $status = $this->scopeConfig->getValue('productinquiry/email_setting/allow_attach_extension',ScopeInterface::SCOPE_STORE);
        return $status;
    }
    public function getCaptchaSiteKey()
    {
    	$site_key = $this->scopeConfig->getValue('msp_securitysuite_recaptcha/general/public_key',ScopeInterface::SCOPE_STORE);
        return $site_key;
    }
    public function getCaptchaSecretKey()
    {
        $secret_key = $this->scopeConfig->getValue('msp_securitysuite_recaptcha/general/private_key',ScopeInterface::SCOPE_STORE);
        return $secret_key;
    }
    public function getCaptchaFormStatusCustomer()
    {
        $captcha_customer = $this->scopeConfig->getValue('customer/captcha/enable', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $captcha_customer;
    }
    public function getCaptchaFormStatusSecurity()
    {
        $captcha_security = $this->scopeConfig->getValue('msp_securitysuite_recaptcha/frontend/enabled',ScopeInterface::SCOPE_STORE);
        return $captcha_security;
    }
    public function getCaptchaFormSelect()
    {
        $selected_form = $this->scopeConfig->getValue('customer/captcha/forms', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $selected_form;
    }
}

