<?php

namespace Dolphin\ProductInquiry\Model;

use Magento\Framework\Data\OptionSourceInterface;

class ProductList implements OptionSourceInterface
{
    protected $_productCollectionFactory;
    protected $_storeManager;

    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_storeManager = $storeManager;
    }
    public function getOptionArray()
    {
        $storeid= $this->_storeManager->getStore();
        $collection = $this->_productCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        $collection->setOrder('name','ASC');
        $collection->addStoreFilter($storeid);
        $options = [];
        foreach ($collection as $product)
        {
            $options[$product->getEntityId()] = $product->getName();
        }
        return $options;
    }
    public function getAllOptions()
    {
        $res = $this->getOptions();
        array_unshift($res, ['value' => '', 'label' => '']);
        return $res;
    }
    public function getOptions()
    {
        $res = [];
        foreach ($this->getOptionArray() as $index => $value) {
            $res[] = ['value' => $index, 'label' => $value];
        }
        return $res;
    }
    public function toOptionArray()
    {
        return $this->getOptions();
    }
}
