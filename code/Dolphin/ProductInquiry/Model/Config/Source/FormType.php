<?php

namespace Dolphin\ProductInquiry\Model\Config\Source;

class FormType implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [
		            ['value' => 'popup', 'label' => __('Popup')],
		            ['value' => 'tab', 'label' => __('Tab')],
               ];
    }
}