<?php

namespace Dolphin\ProductInquiry\Model\Config\Source;

class CaptchaType implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [
		            ['value' => 'captcha', 'label' => __('Captcha')],
		            ['value' => 'recaptcha', 'label' => __('ReCaptcha')],
               ];
    }
}