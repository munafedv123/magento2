<?php

namespace Dolphin\ProductInquiry\Model\ResourceModel\Productinquiry;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';

    protected function _construct()
    {
        $this->_init(
            'Dolphin\ProductInquiry\Model\Productinquiry',
            'Dolphin\ProductInquiry\Model\ResourceModel\Productinquiry'
        );
    }
}
