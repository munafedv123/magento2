<?php

namespace Dolphin\ProductInquiry\Model;

use Magento\Framework\Data\OptionSourceInterface;

class AllowAttachment implements OptionSourceInterface
{
    public function getOptionArray()
    {
        $options = ['0' => __('No'),'1' => __('Yes')];
        return $options;
    }
    public function getAllOptions()
    {
        $res = $this->getOptions();
        array_unshift($res, ['value' => '', 'label' => '']);
        return $res;
    }
    public function getOptions()
    {
        $res = [];
        foreach ($this->getOptionArray() as $index => $value) {
            $res[] = ['value' => $index, 'label' => $value];
        }
        return $res;
    }
    public function toOptionArray()
    {
        return $this->getOptions();
    }
}
