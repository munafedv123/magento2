<?php

namespace Dolphin\ProductInquiry\Model;

use Dolphin\ProductInquiry\Model\ResourceModel\Productinquiry\CollectionFactory;
use Magento\Store\Model\StoreManagerInterface;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    protected $loadedData;
    protected $_storeManager;
    protected $product;

    public function __construct(
        \Magento\Catalog\Model\ProductFactory $product,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $ProductInquiryCollectionFactory,
        array $meta = [],
        array $data = []
    ) {
        $this->product = $product;
        $this->collection = $ProductInquiryCollectionFactory->create();
        $this->_storeManager = $storeManager;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }
    public function getData()
    {
        if (isset($this->loadedData))
        {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        foreach ($items as $model)
        {
            $this->loadedData[$model->getId()] = $model->getData();
            /* file attachament */
            if ($model->getAttechmentFile())
            {
                $m['attechment_file'][0]['name'] = $model->getAttechmentFile();
                //$link_array = explode('/',$m['attechment_file'][0]['name']);
                //$m['attechment_file'][0]['name'] = end($link_array);
                $m['attechment_file'][0]['url'] = $this->getMediaUrl().$model->getAttechmentFile();
                $fullData = $this->loadedData;
                $this->loadedData[$model->getId()] = array_merge($fullData[$model->getId()], $m);
            }
            /* product Name show */
            if ($model->getPrdName())
            {
                $prd_collection = $this->product->create()->load($model->getPrdName());
                $prd_name['prd_name']= $prd_collection->getName();
                $fullData = $this->loadedData;
                $this->loadedData[$model->getId()] = array_merge($fullData[$model->getId()], $prd_name);
            }
        }
        return $this->loadedData;
    }
    public function getMediaUrl()
    {
        $mediaUrl = $this->_storeManager->getStore()
            ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).'Product_Inquiry_Attachments';
        return $mediaUrl;
    }
}
