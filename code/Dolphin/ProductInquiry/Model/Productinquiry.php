<?php

namespace Dolphin\ProductInquiry\Model;

use Dolphin\ProductInquiry\Api\Data\InquiryInterface;

class Productinquiry extends \Magento\Framework\Model\AbstractModel implements InquiryInterface
{
    const CACHE_TAG = 'dolphin_productinquiry_admingrid';

    protected $_cacheTag = 'dolphin_productinquiry_admingrid';
    protected $_eventPrefix = 'dolphin_productinquiry_admingrid';

    protected function _construct()
    {
        $this->_init('Dolphin\ProductInquiry\Model\ResourceModel\Productinquiry');
    }
    public function getId()
    {
        return $this->getData(self::ID);
    }
    public function setId($Id)
    {
        return $this->setData(self::ID, $Id);
    }
    public function getPrdName()
    {
        return $this->getData(self::PRD_NAME);
    }
    public function setPrdName($prd_name)
    {
        return $this->setData(self::PRD_NAME, $prd_name);
    }
    public function getUsrName()
    {
        return $this->getData(self::USR_NAME);
    }
    public function setUsrName($usr_name)
    {
        return $this->setData(self::USR_NAME, $usr_name);
    }
    public function getEmail()
    {
        return $this->getData(self::EMAIL);
    }
    public function setEmail($email)
    {
        return $this->setData(self::EMAIL, $email);
    }
    public function getSubject()
    {
        return $this->getData(self::SUBJECT);
    }
    public function setSubject($subject)
    {
        return $this->setData(self::SUBJECT, $subject);
    }
    public function getInqMsg()
    {
        return $this->getData(self::INQ_MSG);
    }
    public function setInqMsg($inq_msg)
    {
        return $this->setData(self::INQ_MSG, $inq_msg);
    }
    public function getPublishDatetime()
    {
        return $this->getData(self::PUBLISH_DATETIME);
    }
    public function setPublishDatetime($publish_datetime)
    {
        return $this->setData(self::PUBLISH_DATETIME, $publish_datetime);
    }
    public function getUpadteDatetime()
    {
        return $this->getData(self::UPDATE_DATETIME);
    }
    public function setUpadteDatetime($update_datetime)
    {
        return $this->setData(self::UPDATE_DATETIME, $update_datetime);
    }
}