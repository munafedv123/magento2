<?php

namespace Dolphin\ProductInquiry\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Store\Model\ScopeInterface;
use  Magento\Framework\App\Filesystem\DirectoryList;

class Checkcaptcha implements ObserverInterface
{

   protected $_helper;
   protected $_actionFlag;
   protected $messageManager;
   protected $_session;
   protected $_urlManager;
   protected $captchaStringResolver;
   protected $redirect;
   protected $_ProductinquiryFactory;
   protected $_storeManager;
   protected $_transportBuilder;
   protected $uploaderFactory;
   protected $adapterFactory;
   protected $filesystem;
   protected $product;

   public function __construct(
           \Magento\Captcha\Helper\Data $helper,
           \Magento\Framework\App\ActionFlag $actionFlag,
           \Magento\Framework\Message\ManagerInterface $messageManager,
           \Magento\Framework\Session\SessionManagerInterface $session,
           \Magento\Framework\UrlInterface $urlManager,
           \Magento\Framework\App\Response\RedirectInterface $redirect,
           \Magento\Captcha\Observer\CaptchaStringResolver $captchaStringResolver,
           \Magento\Framework\App\RequestInterface $request,
           \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
           \Dolphin\ProductInquiry\Model\ProductinquiryFactory $ProductinquiryFactory,
           \Magento\Store\Model\StoreManagerInterface $storeManager,
           \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
           \Magento\MediaStorage\Model\File\UploaderFactory $uploaderFactory,
           \Magento\Framework\Image\AdapterFactory $adapterFactory,
           \Magento\Framework\Filesystem $filesystem,
           \Magento\Catalog\Model\ProductFactory $product
      ) {
       $this->_helper = $helper;
       $this->_actionFlag = $actionFlag;
       $this->messageManager = $messageManager;
       $this->_session = $session;
       $this->_urlManager = $urlManager;
       $this->redirect = $redirect;
       $this->captchaStringResolver = $captchaStringResolver;
       $this->request = $request;
       $this->scopeConfig = $scopeConfig;
       $this->_ProductinquiryFactory = $ProductinquiryFactory;
       $this->_storeManager = $storeManager;
       $this->_transportBuilder = $transportBuilder;
       $this->uploaderFactory = $uploaderFactory;
       $this->adapterFactory = $adapterFactory;
       $this->filesystem = $filesystem;
       $this->product = $product;
   }
   public function execute(\Magento\Framework\Event\Observer $observer)
   {
      $data = $this->request->getPostValue();
      $captcha_staus = $this->getCaptchaFormStatusCustomer();
      if($captcha_staus == 1)
      {
        /* check captcha is there */
        if(isset($data['captcha']))
        {
          if(is_array($data['captcha']))
          {
            $formId = 'product_inquiry_form';
            $captchaModel = $this->_helper->getCaptcha($formId);
            $controller = $observer->getControllerAction();
            // check magento Captcha Stinrg
            if (!$captchaModel->isCorrect($this->captchaStringResolver->resolve($controller->getRequest(), $formId)))
             {
                $this->messageManager->addError(__('Incorrect CAPTCHA'));
                $this->_actionFlag->set('', \Magento\Framework\App\Action\Action::FLAG_NO_DISPATCH, true);
                $this->_session->setCustomerFormData($controller->getRequest()->getPostValue());
                $url = $this->redirect->getRedirectUrl();
                $controller->getResponse()->setRedirect($url);
                return $this;
             } // if captcha string check
             else
             {
                /* captcha is not set part */
                $showmsg = $this->getSuccessMassage();
                /* check Thank you masssage*/
                if ($showmsg == '')
                {
                  $msg = 'Product Inquiry has been successfully saved.';
                }// if show msg
                else
                {
                  $msg = $showmsg;
                }// if show msg
                ////  file Upload
                $allow_attach_status = $this->getFileAttachmentStatus();
                if($allow_attach_status == 1)
                {
                    try
                      {
                        $uploaderFactory = $this->uploaderFactory->create(['fileId' => 'attechment_file']);
                        // get  allow Extesnion array
                        $attachment_extesion = $this->getFileAttachmentExtension();
                        $attachment_extesion = explode(',', $attachment_extesion);
                        $uploaderFactory->setAllowedExtensions($attachment_extesion);
                        $imageAdapter = $this->adapterFactory->create();
                        $uploaderFactory->setAllowRenameFiles(true);
                        $uploaderFactory->setFilesDispersion(true);
                        $mediaDirectory = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA);
                        $destinationPath = $mediaDirectory->getAbsolutePath('Product_Inquiry_Attachments');
                        $result = $uploaderFactory->save($destinationPath);
                        if (!$result)
                        {
                            throw new LocalizedException(
                                 __('File cannot be saved to path: $1', $destinationPath)
                            );
                         }
                         $data['attechment_file'] = $result['file'];
                      }
                    catch (\Exception $e)
                      {
                        $this->messageManager->addError(__('Attachment not Uplaoded, Please try Agrain'));
                        $this->_session->setCustomerFormData($controller->getRequest()->getPostValue());
                        $url = $this->redirect->getRedirectUrl();
                        $controller->getResponse()->setRedirect($url);
                        return $this;
                      }
                } // allow Attechment
                // save Data
                try
                  {
                    $rowData = $this->_ProductinquiryFactory->create();
                    $rowData->setData($data);
                    $rowData->save();
                    $this->messageManager->addSuccess(__($msg));
                    $this->SendMailObserver($controller);
                  }
                catch (\Exception $e)
                  {
                    $this->messageManager->addError(__('please try again. Form Not Submit'));
                  }
                $this->_session->setCustomerFormData($controller->getRequest()->getPostValue());
                $url = $this->redirect->getRedirectUrl();
                $controller->getResponse()->setRedirect($url);
                return $this;
             } // if captcha string check
          } // chekc captcha value not null
        } // captcha is set or not
      }// customer captcha eneble
   }// main execution part
   public function SendMailObserver($controller) // for email send
   {
      $data = $this->request->getPostValue();
      $store = $this->_storeManager->getStore()->getId();
      $sender = $this->getStoreEmailAddress();
      /* get emamil  Template ID */
      $select_email_template = $this->getEmailTemplate();
      /* copy of Email Address */
      $admin_email_adress = $this->getAdminEmailAddress();
      $email_count= 0;
      if($admin_email_adress != '')
      {
        $admin_email_adress = explode(',', $admin_email_adress);
        $admin_email_copy_to = [];
        foreach ($admin_email_adress as $valid_email)
        {
          $valid_email = preg_replace('/\s+/', '', $valid_email);
          if(filter_var($valid_email, FILTER_VALIDATE_EMAIL))
          {
            $admin_email_copy_to[] = $valid_email;
          }
          $email_count++;
        }
      }
      else
      {
        // admin Email Count
        if($email_count != 0 )
        {
          $this->messageManager->addError(__('Admin Email are Invalid or Empty.'));
          $url = $this->redirect->getRedirectUrl();
          $controller->getResponse()->setRedirect($url);
        }
      }
      // get Product id to name
      $prd_collection = $this->product->create()->load($data['prd_name']);
      $get_prd_name =$prd_collection->getName();
      /* semder send email */
      try
        {
              $transport = $this->_transportBuilder->setTemplateIdentifier($select_email_template)
                ->setTemplateOptions(['area' => 'frontend', 'store' => $store])
                ->setTemplateVars(
                  [
                     'prd_name' => $get_prd_name,
                     'usr_name' => $data['usr_name'],
                     'email'    => $data['email'],
                     'subject'  => $data['subject'],
                     'inq_msg'  => $data['inq_msg'],
                     'admin' =>'admin'
                  ]
                )
                ->setFrom($sender)
                ->addTo($admin_email_copy_to)
                ->getTransport();
              $transport->sendMessage();
        }
    catch (\Exception $e)
        {
              $this->messageManager->addError(__('E-mail send failed, please try again.'));
              $url = $this->redirect->getRedirectUrl();
              $controller->getResponse()->setRedirect($url);
              return $this;
        }
   }// send mail
   public function getSuccessMassage()
   {
     $success_msg = $this->scopeConfig->getValue('productinquiry/general_option/thanks_msg', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
     return $success_msg;
   }
   public function getCaptchaFormStatusCustomer()
   {
      $captcha_customer = $this->scopeConfig->getValue('customer/captcha/enable', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
      return $captcha_customer;
   }
   public function getEmailTemplate()
   {
      $email_template = $this->scopeConfig->getValue('productinquiry/email_setting/email_template', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
      return $email_template;
   }
   public function getAdminEmailAddress()
   {
      $copy_email = $this->scopeConfig->getValue('productinquiry/email_setting/email_copy', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
      return $copy_email;
   }
   public function getStoreEmailAddress()
   {
      $store_email = $this->scopeConfig->getValue('productinquiry/email_setting/store_email_list', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $store_email;
   }
   public function getFileAttachmentStatus()
   {
      $status = $this->scopeConfig->getValue('productinquiry/email_setting/allow_attach',ScopeInterface::SCOPE_STORE);
      return $status;
   }
   public function getFileAttachmentExtension()
   {
        $status = $this->scopeConfig->getValue('productinquiry/email_setting/allow_attach_extension',ScopeInterface::SCOPE_STORE);
        return $status;
   }
}