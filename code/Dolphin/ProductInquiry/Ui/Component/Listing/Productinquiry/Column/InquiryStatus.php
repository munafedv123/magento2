<?php

namespace Dolphin\ProductInquiry\Ui\Component\Listing\Productinquiry\Column;

use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Ui\Component\Listing\Columns\Column;

class InquiryStatus extends Column
{

	public function __construct(
		ContextInterface $context,
		UiComponentFactory $uiComponentFactory,
		array $components = [],
		array $data = []
	) {
		parent::__construct($context, $uiComponentFactory, $components, $data);
	}

	public function prepareDataSource(array $dataSource)
	{
		if (isset($dataSource['data']['items']))
		{
		 	foreach ($dataSource['data']['items'] as $items)
		 	{
		 		if($items['inquiry_status'] == 0)
	 			{
	 				$class = 'minor';
	 			}
	 			elseif ($items['inquiry_status'] == 1)
	 			{
	 			    $class = 'notice';
	 			}
	 			elseif ($items['inquiry_status'] == 2)
	 			{
	 			    $class = 'minor';
	 			}
	 			elseif ($items['inquiry_status'] == 3)
	 			{
	 			    $class = 'notice';
	 			}
	 			elseif ($items['inquiry_status'] == 4)
	 			{
	 			    $class = 'critical';
	 			}
	 			elseif ($items['inquiry_status'] == 5)
	 			{
	 			    $class = 'minor';
	 			}
	 			else
	 			{
	 				 $class = 'minor';
	 			}
            	$items['inquiry_status'] = '<span class="grid-severity-'.$class .'" >' . $items['inquiry_status'] . '</span>';
			}
		}
		// echo "<pre>";
		// print_r($dataSource);
		// exit();
		return $dataSource;
	}
}