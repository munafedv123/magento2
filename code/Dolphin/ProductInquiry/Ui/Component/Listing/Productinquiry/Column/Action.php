<?php

namespace Dolphin\ProductInquiry\Ui\Component\Listing\Productinquiry\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\UrlInterface;

class Action extends \Magento\Ui\Component\Listing\Columns\Column
{
    protected $urlBuilder;

    const ROW_EDIT_URL = 'productinquiry/productinquiry/editinquiry';
    const ROW_VIEW_URL = 'productinquiry/productinquiry/viewinquiry';
    const ROW_DELETE_URL = 'productinquiry/productinquiry/delete';

    public function __construct(
        \Magento\Framework\UrlInterface $urlBuilder,
        \Magento\Framework\View\Element\UiComponent\ContextInterface $context,
        \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                if (isset($item['id'])) {
                     $delete_url = $this->urlBuilder->getUrl(static::ROW_DELETE_URL,['id' => $item['id']]);
                    $item[$this->getData('name')] = [
                         'view' => [
                            'href' => $this->urlBuilder->getUrl(
                                static::ROW_VIEW_URL,
                                [
                                    'id' => $item['id']
                                ]
                            ),
                            'label' => __('View')
                        ],
                        'edit' => [
                            'href' => $this->urlBuilder->getUrl(
                                static::ROW_EDIT_URL,
                                [
                                    'id' => $item['id']
                                ]
                            ),
                            'label' => __('Edit & Replay')
                        ],
                        'delete' => [
                            'href' => "javascript:deleteConfirm('Are you sure you want to do this row ?','".$delete_url."')",
                            'label' => __('Delete')
                        ],
                    ];
                }
            }
        }
      return $dataSource;
    }
}
