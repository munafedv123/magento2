<?php

namespace Dolphin\ProductInquiry\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $tableName = $setup->getTable('dolphin_productinquiry_admingrid');
        if (version_compare($context->getVersion(), '1.0.1', '<')) {
              if ($setup->getConnection()->isTableExists($tableName) == true) {
                $connection = $setup->getConnection();
                $connection->changeColumn(
                    $tableName,
                    'inq_msg',
                    'inq_msg',
                    ['type' => Table::TYPE_TEXT, 'nullable' => false, 'default' => ''],
                    'Product Inquiry Massage'
                );
            }
        }
        if (version_compare($context->getVersion(), '1.0.2', '<')) {
              if ($setup->getConnection()->isTableExists($tableName) == true) {
                $connection = $setup->getConnection();
                $connection->dropColumn($tableName, 'prd_sku');
            }
        }
        if(version_compare($context->getVersion(), '1.0.8', '<')) {
            $setup->getConnection()->addColumn(
                $setup->getTable($tableName),
                'allow_attechment',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => false,
                    'comment' => 'Allow Attechment File',
                    'after' => 'inq_msg'
                ]
            );
        }
        if(version_compare($context->getVersion(), '2.0.9', '<')) {
            $setup->getConnection()->addColumn(
                $setup->getTable($tableName),
                'attechment_file',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => false,
                    'comment' => 'Attechment File Name',
                    'after' => 'allow_attechment'
                ]
            );
        }
        if (version_compare($context->getVersion(), '2.1.10', '<')) {
              if ($setup->getConnection()->isTableExists($tableName) == true) {
                $connection = $setup->getConnection();
                $connection->changeColumn(
                    $tableName,
                    'attechment_file',
                    'attechment_file',
                    ['type' => Table::TYPE_TEXT, 'nullable' => true, 'default' => ''],
                    'Attechment File Name'
                );
            }
        }
        if(version_compare($context->getVersion(), '2.2.0', '<')) {
            $setup->getConnection()->addColumn(
                $setup->getTable($tableName),
                'inquiry_replay',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'Inquiry Replay',
                    'after' => 'attechment_file'
                ]
            );
        }
        if(version_compare($context->getVersion(), '2.2.1', '<')) {
            $setup->getConnection()->addColumn(
                $setup->getTable($tableName),
                'inquiry_status',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'Inquiry Status',
                    'after' => 'inquiry_replay'
                ]
            );
        }
        $setup->endSetup();
    }
}