<?php

namespace Dolphin\Imageflip\Model\Config\Source;

class Flipimage implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [['value' => 'Y', 'label' => __('Horizontal')],['value' => 'X', 'label' => __('Vertical')]];
    }

    public function toArray()
    {
        return ['horizontal' => __('Horizontal'),'vertical' => __('Vertical')];
    }
}
