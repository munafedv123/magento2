<?php
namespace Dolphin\Imageflip\Plugin;

class ProductList
{   
    protected $layout;

    public function __construct(
        \Magento\Framework\View\LayoutInterface $layout
    ) {
        $this->layout = $layout;
    }

    public function aroundGetProductDetailsHtml(
        \Magento\Catalog\Block\Product\ListProduct $subject,
        \Closure $proceed,
        \Magento\Catalog\Model\Product $product
    ) {
        return $this->layout->createBlock('Dolphin\Imageflip\Block\Imageflip')->setProduct($product)->setTemplate('Dolphin_Imageflip::imageflip.phtml')->toHtml();
    }               
}
