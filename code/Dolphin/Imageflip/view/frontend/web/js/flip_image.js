define(["jquery", "flipimage"], function($,flipimage){
	
	$(document).ready( function() {
	
	$(".product-items").children("li").each(function() {
	
	var imageclass = ".product-image-photo";
    //var imageflip = "Y";
    var imageflip = $(this).find('.flipimage').attr('flipvalue');
    
    var flipper1 = $(this).find('.flipimage').attr('name', 'flipid').text();

     if (flipper1 != '') { 
        $(this).find('.flipimage').attr('name', 'flipid').closest('.product-item-info')
          .on("mouseenter", function() {

          var base = $(this).find('.flipimage').attr('name', 'flipid').closest('.product-item-info')
          .find('.product-image-photo').attr('src');
          var flipper =$(this).find('.flipimage').attr('name', 'flipid').text();
              
          $(this).find('.flipimage').attr('name', 'flipid').text(base);
			if (imageflip == 'X') {
								console.log('x vale');
								$(this).find('.flipimage').attr('name', 'flipid').closest('.product-item-info')
								.find(imageclass).css({
									'transform': 'rotate' + imageflip + '(-360deg)',
									'transform-style': 'preserve-3d',
									'transition': 'all 0.5s ease-out 0s'
							});
							}
							if (imageflip == 'Y') {
								console.log('y vale');
								$(this).find('.flipimage').attr('name', 'flipid').closest('.product-item-info')
								.find(imageclass).css({
									'transform': 'rotate' + imageflip + '(180deg)',
										'transform-style': 'preserve-3d',
										'transition': 'all 0.5s ease-out 0s'
							});
							}

            $(this).find('.flipimage').attr('name', 'flipid').closest('.product-item-info')
              .find(imageclass).attr('src', flipper);
              }).on("mouseleave", function() {

          var base =  $(this).find('.flipimage').attr('name', 'flipid').closest('.product-item-info')
          .find('.product-image-photo').attr('src');
          var flipper =$(this).find('.flipimage').attr('name', 'flipid').text();
           $(this).find('.flipimage').attr('name', 'flipid').text(base);
              
           $(this).find('.flipimage').attr('name', 'flipid').closest('.product-item-info')
              .find(imageclass).css({
              'transform': 'rotate' + imageflip + '(0deg)',
              'transform-style': 'preserve-3d',
              'transition': 'all 0.5s ease-out 0s'
            });

           $(this).find('.flipimage').attr('name', 'flipid').closest('.product-item-info')
              .find(imageclass).attr('src', flipper);
            });
        }
  
});
});

})
