<?php
namespace Dolphin\Imageflip\Block;

class Imageflip extends \Magento\Framework\View\Element\Template
{
    protected $helperData;
    protected $_storeManager;

    public function __construct(

        \Dolphin\Imageflip\Helper\Data $helperData,
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->_registry = $registry;
        $this->helperData = $helperData;
        $this->_storeManager = $storeManager;
        parent::__construct($context);
    }

    public function isEnable()
    {

        return $this->helperData->getEnable();
    }
    public function getFlipvalue()
    {

        return $this->helperData->getFlipimage();
    }
    public function getMediaUrl()
    {
        $mediaUrl = $this->_storeManager
            ->getStore()
            ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        return $mediaUrl;
    }
}
