<?php

namespace Dolphin\Imageflip\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
   
    
    const XML_IMAGEFLIPSEC_FLIPIMAGE = 'imageflipsec/settings/flipimage';
    const XML_IMAGEFLIPSEC_ENABLE = 'imageflipsec/settings/enabled';
   

    public function getFlipimage()
    {
        return $this->getConfig(self::XML_IMAGEFLIPSEC_FLIPIMAGE);
    }
    
    public function getEnable()
    {
        return $this->getConfig(self::XML_IMAGEFLIPSEC_ENABLE);
    }

  
    public function getConfig($path)
    {
        return $this->scopeConfig->getValue(
            $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
    
}
