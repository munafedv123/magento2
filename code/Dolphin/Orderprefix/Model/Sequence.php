<?php
namespace Dolphin\Orderprefix\Model;

use Magento\Framework\App\ResourceConnection as AppResource;
use Magento\SalesSequence\Model\Meta;
use Magento\SalesSequence\Model\ResourceModel\Profile;
use Magento\Store\Model\ScopeInterface;

class Sequence extends \Magento\SalesSequence\Model\Sequence
{

    /**
     * @var string
     */
    private $lastIncrementId;

    protected $storeManager;

    /**
     * @var Meta
     */
    private $meta;

    protected $profile;

    /**
     * @var false|\Magento\Framework\DB\Adapter\AdapterInterface
     */
    private $connection;

    /**
     * @var string
     */
    private $pattern;

    /**
     * @param Meta $meta
     * @param AppResource $resource
     * @param string $pattern
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        Profile $profile,
        Meta $meta,
        AppResource $resource,
        $pattern = self::DEFAULT_PATTERN
    ) {

        $this->meta = $meta;
        $this->connection = $resource->getConnection('sales');
        $this->pattern = $pattern;
        $this->profile = $profile;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        parent::__construct($meta, $resource, $pattern);
    }

    /**
     * Retrieve current value
     *
     * @return string
     */

    public function getCurrentValue()
    {
        if (!isset($this->lastIncrementId)) {
            return null;
        }

        $storeId = $this->meta->getData('store_id');
        $order = $this->scopeConfig->
        getValue('add_prefix/prefix/order', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
        $invoice = $this->scopeConfig->
        getValue('add_prefix/prefix/invoice', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
        $shipment = $this->scopeConfig->
        getValue('add_prefix/prefix/shipment', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
        $creditmemo = $this->scopeConfig->
        getValue('add_prefix/prefix/creditmemo', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);

        $metaEntityType = $this->meta->getEntityType();

        if ($metaEntityType == 'order') {

            $prefix = $order;
        }

        if ($metaEntityType == 'invoice') {
            $prefix = $invoice;
        }
        if ($metaEntityType == 'shipment') {
            $prefix = $shipment;
        }
        if ($metaEntityType == 'creditmemo') {
            $prefix = $creditmemo;
        } else {
            $this->meta->getActiveProfile()->getPrefix();
        }

        return sprintf(
            $this->pattern,
            $prefix,
            $this->calculateCurrentValue(),
            $this->meta->getActiveProfile()->getSuffix()
        );
    }

    /**
     * Retrieve next value
     *
     * @return string
     */
    public function getNextValue()
    {
        $this->connection->insert($this->meta->getSequenceTable(), []);
        $this->lastIncrementId = $this->connection->lastInsertId($this->meta->getSequenceTable());
        return $this->getCurrentValue();
    }

    /**
     * Calculate current value depends on start value
     *
     * @return string
     */
    private function calculateCurrentValue()
    {
        return ($this->lastIncrementId - $this->meta->getActiveProfile()->getStartValue())
         * $this->meta->getActiveProfile()->getStep() + $this->meta->getActiveProfile()->getStartValue();
    }
}
