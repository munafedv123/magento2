<?php

namespace Dolphin\Orderprefix\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * Admin config settings
     */
  
    const XML_ORDER_PREFIX = 'add_prefix/prefix/order';
    const XML_INVOICE_PREFIX = 'add_prefix/prefix/invoice';
    const XML_SHIPMENT_PREFIX = 'add_prefix/prefix/shipment';
    const XML_CREDITMEMO_PREFIX = 'add_prefix/prefix/creditmemo';

    public function getOrderPrefix()
    {
        return $this->getConfig(self::XML_ORDER_PREFIX);
    }
    
    public function getInvoicePrefix()
    {
        return $this->getConfig(self::XML_INVOICE_PREFIX);
    }

    public function getShipmentPrefix()
    {
        return $this->getConfig(self::XML_SHIPMENT_PREFIX);
    }
    
    public function getCreditmemoPrefix()
    {
        return $this->getConfig(self::XML_CREDITMEMO_PREFIX);
    }
    public function getConfig($path)
    {
        return $this->scopeConfig->getValue(
            $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}
