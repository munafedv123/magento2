# Mage2 Module Dolphin Orderprefix

    ``dolphin/module-orderprefix``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities


## Installation
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/Dolphin`
 - Enable the module by running `php bin/magento module:enable Dolphin_Orderprefix`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require dolphin/module-orderprefix`
 - enable the module by running `php bin/magento module:enable Dolphin_Orderprefix`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`


## Configuration

 - order_prefix (add_prefix/order_prefix/order_prefix)

 - invoice_prefix (add_prefix/invoice_prefix/invoice_prefix)

 - shipment_prefix (add_prefix/shipment_prefix/shipment_prefix)

 - creditmemoid_prefix (add_prefix/creditmemoid_prefix/creditmemoid_prefix)


## Specifications




## Attributes



